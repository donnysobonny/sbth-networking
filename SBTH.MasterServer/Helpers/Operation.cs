﻿using System;
using Photon.SocketServer;
using SBTH.Common.Enums;
using SBTH.Server.Helpers;
using System.Collections.Generic;
using ExitGames.Logging;
using System.Collections.Specialized;

namespace SBTH.MasterServer.Helpers {
    /// <summary>
    /// The operation handler is a helper class designed to handle operations. Once creating an instance of the OperationHandler class, you are able to call any handler methods.
    /// </summary>
    public class Operation : OperationBase {

        private ILogger log = LogManager.GetCurrentClassLogger();

        public Operation(PeerBase peer, OperationRequest operationRequest, SendParameters sendParameters) : base(peer, operationRequest, sendParameters) { }

        public override void handle() {
            try {
                switch (operationRequest.OperationCode) {
                    case (byte)OperationCode.subRequestRemoveSystem:
                        this.subRequestRemoveSystem();
                        break;
                    case (byte)OperationCode.peerRequstSubBySystem:
                        this.peerRequstSubBySystem();
                        break;
                    default:
                        //unrecognised request
                        this.peer.SendOperationResponse(new OperationResponse() {
                            OperationCode = operationRequest.OperationCode,
                            ReturnCode = (short)ReturnCode.OP_NOT_RECOGNISED,
                            DebugMessage = "Operation not recognised"
                        }, sendParameters);
                        break;
                }
            } catch (Exception ex) {
                //auto send error
                this.peer.SendOperationResponse(new OperationResponse() {
                    OperationCode = operationRequest.OperationCode,
                    ReturnCode = (short)ReturnCode.GENERAL_ERROR,
                    DebugMessage = ex.ToString()
                }, sendParameters);
            }
        }

        #region handlers

        private void subRequestRemoveSystem() {
            //system id is required
            if(!this.parameterExists(ParameterKey.systemId)) {
                throw new Exception("system id is required");
            }
            int systemId = this.getParameter<int>(ParameterKey.systemId);

            Application app = Application.get();
            int expireAfter = 0;
            lock(app.systemKey) {
                //have we sent a peer to that system in the last 30 seconds?
                if(app.systemLastPeered.ContainsKey(systemId)) {
                    DateTime time = app.systemLastPeered[systemId];
                    if(time.AddSeconds(30) > DateTime.Now) {
                        //tell the server to expire in 30 seconds
                        expireAfter = 30;
                    }
                }

                //if expireAfer is zero, we can remove the reference
                if(expireAfter == 0) {
                    //remove the systemToServer ref
                    app.systemToServer.Remove(systemId);
                    //remove the systemLastPeered ref
                    app.systemLastPeered.Remove(systemId);
                    //in the serverToSystems list, remove the system
                    List<int> systemIds = app.serverToSystems[this.peer.ConnectionId];
                    systemIds.Remove(systemId);
                    app.serverToSystems[this.peer.ConnectionId] = systemIds;
                }
            }
            //send a response with the expireAfter
            Dictionary<byte, object> p = new Dictionary<byte, object>();
            p.Add((byte)ParameterKey.expireInSeconds, expireAfter);
            this.peer.SendOperationResponse(new OperationResponse() {
                OperationCode = this.operationRequest.OperationCode,
                Parameters = p,
            }, this.sendParameters);
        }

        private void peerRequstSubBySystem() {
            //system id required
            if(!this.parameterExists(ParameterKey.systemId)) {
                throw new Exception("system id is requied");
            }
            int systemId = this.getParameter<int>(ParameterKey.systemId);

            Application app = Application.get();
            lock(app.systemKey) {
                //is a system assigne to a server?
                ServerPeer serverPeer;
                if(app.systemToServer.ContainsKey(systemId)) {
                    serverPeer = app.serverPeers[app.systemToServer[systemId]];
                    //update the systemLastPeered entry
                    app.systemLastPeered[systemId] = DateTime.Now;
                } else {
                    //we need to assign the system to a server, based on the server with the least load
                    Dictionary<int, int> serverLoad = new Dictionary<int, int>(app.serverLoad);
                    int serverId = 0;
                    foreach(KeyValuePair<int, int> pair in serverLoad) {
                        serverId = pair.Key;
                        break;
                    }
                    serverPeer = app.serverPeers[serverId];

                    //assign this server
                    app.systemToServer[systemId] = serverId;
                    //create an entry in systemLastPeered
                    app.systemLastPeered[systemId] = DateTime.Now;
                    //finally, add the system to the server's serverToSystems list
                    List<int> systemIds = app.serverToSystems[serverId];
                    systemIds.Add(systemId);
                    app.serverToSystems[serverId] = systemIds;
                }
                if(serverPeer == null) {
                    throw new Exception("Unable to locate server for specified system id");
                }

                //respond to the player peer with the server's IP to connect to
                Dictionary<byte, object> p = new Dictionary<byte, object>();
                p.Add((byte)ParameterKey.serverIP, serverPeer.RemoteIP);
                this.peer.SendOperationResponse(new OperationResponse() {
                    OperationCode = this.operationRequest.OperationCode,
                    Parameters = p
                }, this.sendParameters);
            }
        }

        #endregion

        private void simpleResponse() {
            this.peer.SendOperationResponse(new OperationResponse() {
                OperationCode = this.operationRequest.OperationCode
            }, this.sendParameters);
        }
    }
}