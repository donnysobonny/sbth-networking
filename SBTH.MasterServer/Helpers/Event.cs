﻿using Photon.SocketServer;
using SBTH.Server.Helpers;

namespace SBTH.MasterServer.Helpers {
    public class Event : EventBase {
        public Event(EventData eventData, SendParameters sendParameters) : base(eventData, sendParameters, Application.get()) { }
    }
}
