﻿using Photon.SocketServer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PhotonHostRuntimeInterfaces;
using ExitGames.Logging;
using SBTH.MasterServer.Helpers;

namespace SBTH.MasterServer {
    public class PlayerPeer : PeerBase {

        private readonly ILogger log = LogManager.GetCurrentClassLogger();

        public PlayerPeer(InitRequest initRequest)
            : base(initRequest.Protocol, initRequest.PhotonPeer)
        {
            
        }

        protected override void OnDisconnect(DisconnectReason reasonCode, string reasonDetail) {
            
        }

        protected override void OnOperationRequest(OperationRequest operationRequest, SendParameters sendParameters) {
            log.DebugFormat("Operation request from player peer ({0}). {1}", this.ConnectionId, operationRequest.OperationCode);

            //simply handle the operation
            new Operation(this, operationRequest, sendParameters).handle();
        }
    }
}
