﻿using Photon.SocketServer.ServerToServer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Photon.SocketServer;
using PhotonHostRuntimeInterfaces;
using ExitGames.Logging;
using SBTH.Common.Enums;
using SBTH.MasterServer.Helpers;

namespace SBTH.MasterServer {
    public class ServerPeer : ServerPeerBase {

        private static readonly ILogger log = LogManager.GetCurrentClassLogger();

        public ServerPeer(InitRequest initRequest)
            : base(initRequest.Protocol, initRequest.PhotonPeer) {
            log.InfoFormat("game server connection from {0}:{1} established (id={2})", this.RemoteIP, this.RemotePort, this.ConnectionId);
        }

        protected override void OnDisconnect(DisconnectReason reasonCode, string reasonDetail) {
            log.WarnFormat("Game server {0} disconnected! {1} - {2}", this.ConnectionId, reasonCode, reasonDetail);

            //disconnect from application
            Application app = Application.get();
            //remove the peer and associated data
            if (app.serverPeers.ContainsKey(this.ConnectionId)) {
                lock (app.peerKey) {
                    lock(app.systemKey) {
                        app.serverPeers.Remove(this.ConnectionId);
                        app.serverLoad.Remove(this.ConnectionId);

                        List<int> systemIds = app.serverToSystems[this.ConnectionId];
                        app.serverToSystems.Remove(this.ConnectionId);
                        systemIds.ForEach(delegate (int systemId) {
                            app.systemToServer.Remove(systemId);
                            app.systemLastPeered.Remove(systemId);
                        });
                    }
                }
            } 
        }

        protected override void OnEvent(IEventData eventData, SendParameters sendParameters) {
            log.DebugFormat("Received event from sub server ({0}): code={1}", this.ConnectionId, eventData.Code);

            switch(eventData.Code) {
                case (byte)EventCode.sendSubToMasterUpdate:
                    //record the server's load
                    if(eventData.Parameters.ContainsKey((byte)ParameterKey.serverLoad)) {
                        log.DebugFormat("Server {0} tells us that their load is {1}", this.ConnectionId, eventData.Parameters[(byte)ParameterKey.serverLoad]);

                        //update the serverLoad
                        Application app = (Application)Photon.SocketServer.ApplicationBase.Instance;
                        if (app.serverPeers.ContainsKey(this.ConnectionId)) {
                            lock (app.peerKey) {
                                app.serverLoad[this.ConnectionId] = (int)eventData.Parameters[(byte)ParameterKey.serverLoad];
                                //sort the dict
                                //app.serverLoad = app.serverLoad.OrderBy(x => x.Value).ToDictionary(x => x.Key, x => x.Value);
                            }
                        } else {
                            log.DebugFormat("Server {0} trying to update load but they are not in the serverPeers list", this.ConnectionId);
                        }
                    }
                    break;
            }
        }

        protected override void OnOperationRequest(OperationRequest operationRequest, SendParameters sendParameters) {
            log.DebugFormat("Request from sub server {0}: {1}", this.ConnectionId, operationRequest.OperationCode);

            //handle
            new Operation(this, operationRequest, sendParameters).handle();
        }

        protected override void OnOperationResponse(OperationResponse operationResponse, SendParameters sendParameters) {
            
        }
    }
}
