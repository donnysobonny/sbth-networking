﻿using ExitGames.Logging;
using ExitGames.Logging.Log4Net;
using log4net.Config;
using Photon.SocketServer;
using SBTH.Common.Enums;
using System;
using System.Collections.Generic;
using System.IO;

namespace SBTH.MasterServer {
    public class Application : ApplicationBase {

        private static readonly ILogger log = LogManager.GetCurrentClassLogger();
        
        /// <summary>
        /// A list of sub server peers. Modifications to this list should lock the peerKey
        /// </summary>
        public Dictionary<int, ServerPeer> serverPeers = new Dictionary<int, ServerPeer>();

        /// <summary>
        /// Here we keep a record of sub servers and their current load as we receive updates, so that we can load balance
        /// </summary>
        public Dictionary<int, int> serverLoad = new Dictionary<int, int>();

        /// <summary>
        /// This is a record of system => server, storing which server is currently assigned to which system (if any).
        /// </summary>
        public Dictionary<int, int> systemToServer = new Dictionary<int, int>();
        /// <summary>
        /// This is a record of server => systems, storing the systems that a server is currently handling (if any).
        /// </summary>
        public Dictionary<int, List<int>> serverToSystems = new Dictionary<int, List<int>>();
        /// <summary>
        /// This is a record of when each system was last sent a peer. We use this to decide whether a sub server can close, cleanup and unassign itself from a system while avoiding cross-over issues
        /// </summary>
        public Dictionary<int, DateTime> systemLastPeered = new Dictionary<int, DateTime>();

        public readonly object peerKey = new object();
        public readonly object systemKey = new object();

        public static Application get() {
            return (Application)Photon.SocketServer.ApplicationBase.Instance;
        }

        protected override PeerBase CreatePeer(InitRequest initRequest) {
            //we decipher whether the init request is coming from a game server or a player by looking at the port it used
            //since game server init requests happen on the tcp port: 4520
            if (initRequest.LocalPort == 4520) {
                lock(this.peerKey) {
                    //game server
                    ServerPeer serverPeer = new ServerPeer(initRequest);
                    //store the server peer in the collection
                    this.serverPeers[serverPeer.ConnectionId] = serverPeer;
                    //create a load entry with maximum until the server tells us otherwise
                    this.serverLoad[serverPeer.ConnectionId] = (int)ServerLoad.excessive;
                    //create a new entry for serverToSystems
                    this.serverToSystems[serverPeer.ConnectionId] = new List<int>();

                    log.DebugFormat("Game server connected: {0}", serverPeer.ConnectionId);
                    log.DebugFormat("server peer count {0}", this.serverPeers.Count);
                    return serverPeer;
                }                
            } else {
                //player peer
                return new PlayerPeer(initRequest);
            }
        }

        protected override void Setup() {
            log4net.GlobalContext.Properties["Photon:ApplicationLogPath"] = Path.Combine(this.ApplicationRootPath, "log");

            // log4net
            string path = Path.Combine(this.BinaryPath, "log4net.config");
            var file = new FileInfo(path);
            if (file.Exists) {
                LogManager.SetLoggerFactory(Log4NetLoggerFactory.Instance);
                XmlConfigurator.ConfigureAndWatch(file);
            }

            log.InfoFormat("Created master server application: type={0}", Instance.GetType());
        }

        protected override void TearDown() {

        }
    }
}
