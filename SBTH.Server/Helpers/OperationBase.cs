﻿using Photon.SocketServer;
using SBTH.Common.Enums;
using System.Collections.Generic;

namespace SBTH.Server.Helpers {
    public abstract class OperationBase {

        protected PeerBase peer;
        protected OperationRequest operationRequest;
        protected SendParameters sendParameters;

        public OperationBase(PeerBase peer, OperationRequest operationRequest, SendParameters sendParameters) {
            this.peer = peer;
            this.operationRequest = operationRequest;
            this.sendParameters = sendParameters;
        }

        public abstract void handle();

        protected void sendSimpleResponse() {
            this.peer.SendOperationResponse(new OperationResponse() {
                OperationCode = this.operationRequest.OperationCode
            }, this.sendParameters);
        }

        protected void sendError(string error, ReturnCode returnCode) {
            this.peer.SendOperationResponse(new OperationResponse() {
                OperationCode = this.operationRequest.OperationCode,
                ReturnCode = (byte)returnCode,
                DebugMessage = error
            }, this.sendParameters);
        }

        protected void sendSimpleError(string error) {
            this.peer.SendOperationResponse(new OperationResponse() {
                OperationCode = this.operationRequest.OperationCode,
                ReturnCode = (byte)ReturnCode.GENERAL_ERROR,
                DebugMessage = error
            }, this.sendParameters);
        }

        protected void sendUnknownOperation() {
            this.peer.SendOperationResponse(new OperationResponse() {
                OperationCode = this.operationRequest.OperationCode,
                ReturnCode = (byte)ReturnCode.OP_NOT_RECOGNISED,
                DebugMessage = "Unkown operation"
            }, this.sendParameters);
        }

        protected bool parameterExists(ParameterKey parameterKey) {
            return this.operationRequest.Parameters.ContainsKey((byte)parameterKey);
        }

        protected T getParameter<T>(ParameterKey parameterKey) {
            return (T)this.operationRequest.Parameters[(byte)parameterKey];
        }

        public Dictionary<byte, object> getCompressed() {
            Dictionary<byte, object> p = this.operationRequest.Parameters;
            //add the operation code
            p.Add((byte)ParameterKey.operationCode, this.operationRequest.OperationCode);
            return p;
        }
    }
}
