﻿using System.Collections.Generic;
using Photon.SocketServer;
using SBTH.Common.Enums;

namespace SBTH.Server.Helpers {
    public abstract class EventBase {

        protected ApplicationBase application;
        protected EventData eventData;
        protected SendParameters sendParameters;

        public EventBase(EventData eventData, SendParameters sendParameters, ApplicationBase application) {
            this.application = application;
            this.eventData = eventData;
            this.sendParameters = sendParameters;
        }

        public void handle(PeerBase targetPeer) {
            targetPeer.SendEvent(this.eventData, this.sendParameters);
        }

        public void handle(Dictionary<int, PeerBase> targetPeers) {
            foreach (KeyValuePair<int, PeerBase> pair in targetPeers) {
                pair.Value.SendEvent(this.eventData, this.sendParameters);
            }
        }

        public void broadcast(Dictionary<int, PeerBase> targetPeers) {
            List<PeerBase> peers = new List<PeerBase>();
            foreach (KeyValuePair<int, PeerBase> pair in targetPeers) {
                peers.Add(pair.Value);
            }
            this.application.BroadCastEvent(this.eventData, peers, this.sendParameters);
        }

        public Dictionary<byte, object> getCompressed() {
            Dictionary<byte, object> p = this.eventData.Parameters;
            //add the event code
            p[(byte)ParameterKey.eventCode] = this.eventData.Code;
            return p;
        }
    }
}
