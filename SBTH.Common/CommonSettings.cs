﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace SBTH.Common {
    public static class CommonSettings {
        public static string masterServerAddress = "localhost:5055";
        public static string masterServerApplication = "SBTH.MasterServer";

        public static string subServerPort = "5056";
        public static string subServerApplication = "SBTH.SubServer";

        public static float real_time_update_interval = 0.03f;
        public static float high_priority_update_interval = 0.03f;
        public static float low_priority_update_interval = 0.3f;

        public static float ship_positional_correctional_distance = 1f;
        public static float ship_rotational_correctional_distance = 30f;
        public static float ship_persist_interval = 5f;
        public static float ship_max_y_angle = 80f;
        public static float ship_camera_reset_time = 2f;
        public static float ship_camera_reset_speed = 4f;
        public static float ship_fighter_mode_rotation_lerp_threshold = 40f;
        public static float ship_rotation_lerp_speed = 0.06f;
        public static float ship_min_camera_perspective = 40f;
        public static float ship_max_camera_perspective = 60f;
        public static float ship_camera_perpective_lerp = 6f;
        public static float ship_camera_straffe_offset_lerp = 6f;
        public static float ship_camera_zoom_step_time = 0.5f;
        public static float ship_camera_zoom_steps = 10f;
        public static float ship_camera_zoom_lerp = 0.2f;
        public static float ship_camera_dist_flare_mult = 3.5f;
        public static float ship_camera_dist_starfield_brightness_mult = 1.5f;
        public static float ship_warp_angle_offset = 30f;
        public static float ship_warp_countdown_time = 3f;

        public static float ship_weapon_rotation_lerp = 100f;
        
        public static float warping_distance_mult = 0.05f;
        public static float warping_max_speed = 4000000f;

        public static float landmark_default_landing_distance = 100f;
        
        public static float celestial_sun_scale = 1000000f;
        public static float celestial_sun_distance = 1500000f;
        public static float celestial_planet_scale = 100000f;
        public static float celestial_planet_distance = 200000f;
        public static float celestial_moon_scale = 80000f;
        public static float celestial_moon_distance = 400000f;

        public static float solar_system_scale = 50000000f;
    }
}
