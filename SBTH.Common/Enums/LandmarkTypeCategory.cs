﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SBTH.Common.Enums {
    public static class LandmarkTypeCategory {
        public const string sun = "sun";
        public const string planet = "planet";
        public const string moon = "moon";
        public const string jump_bridge = "jump_bridge";
        public const string asteroid_field = "asteroid_field";
        public const string rebel_instance = "rebel_instance";
        public const string xiloform_instance = "xiloform_instance";
    }
}
