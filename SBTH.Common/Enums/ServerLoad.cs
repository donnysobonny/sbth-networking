﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace SBTH.Common.Enums {
    public enum ServerLoad {
        excessive = 4,
        high = 3,
        moderate = 2,
        low = 1,
        minimal = 0
    }
}
