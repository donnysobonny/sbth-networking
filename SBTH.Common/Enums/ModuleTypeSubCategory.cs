﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SBTH.Common.Enums {
    public static class ModuleTypeSubCategory {
        public const string none = "none";
        public const string enhancers = "enhancers";
        public const string injectors = "injectors";
        public const string hardeners = "hardeners";
        public const string maintainers = "maintainers";
        public const string engineering = "engineering";
        public const string firewall_modules = "firewall_modules";
        public const string upgrade_modules = "upgrade_modules";
        public const string cannons = "cannons";
        public const string combat_lasers = "combat_lasers";
        public const string disruptor_launchers = "disruptor_launchers";
        public const string missile_launchers = "missile_launchers";
        public const string scan_probe_launchers = "scan_probe_launchers";
        public const string utility_lasers = "utility_lasers";
    }
}
