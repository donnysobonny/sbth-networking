﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace SBTH.Common.Enums {
    public enum OperationCode : byte {

        /// <summary>
        /// RPC request
        /// </summary>
        RPC = 254,

        /// <summary>
        /// A peer connected to master, looking to be directed to the correct sub server based on a system
        /// </summary>
        peerRequstSubBySystem = 251,
        /// <summary>
        /// A sub server has requested to cleanup and unassign itself from a system due to inactivity
        /// </summary>
        subRequestRemoveSystem = 250,

        /// <summary>
        /// A peer is asking a sub server to create/join a room
        /// </summary>
        createOrJoinRoom = 249,
        /// <summary>
        /// A peer is leaving a room
        /// </summary>
        leaveRoom = 248,

        setPlayerData = 247,

        addItemToRoom = 246,
        removeItemFromRoom = 245,

        realTimeUpdate = 244,
        highPriorityUpdate = 243,
        lowPriorityUpdate = 242
    }
}
