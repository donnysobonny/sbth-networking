﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SBTH.Common.Enums {
    public static class ItemTypeSubCategory {
        public const string container = "container";
        public const string jump_beacon = "jump_beacon";
        public const string jump_gate = "jump_gate";
        public const string space_station = "space_station";
        public const string turret = "turret";
        public const string assault = "assault";
        public const string carrier = "carrier";
        public const string disruptor = "disruptor";
        public const string explorer = "explorer";
        public const string guardian = "guardian";
        public const string hauler = "hauler";
        public const string interceptor = "interceptor";
        public const string prospector = "prospector";
        public const string welder = "welder";
    }
}
