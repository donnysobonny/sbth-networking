﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SBTH.Common.Enums {
    public static class ItemTypeCategory {
        public const string ships = "ships";
        public const string deployables = "deployables";
        public const string ores = "ores";
        public const string metals = "metals";
        public const string asteroid = "asteroid";
        public const string ship_wreck = "ship_wreck";
        public const string @static = "static";
    }
}
