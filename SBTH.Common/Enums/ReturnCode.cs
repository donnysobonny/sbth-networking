﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace SBTH.Common.Enums {
    public enum ReturnCode : short {
        OK = 0,
        GENERAL_ERROR = 1,
        OP_NOT_RECOGNISED = 2,
        OBJECT_NOT_CREATED = 3,
    }
}
