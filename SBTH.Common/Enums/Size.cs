﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SBTH.Common.Enums {
    public static class Size {
        public const string XS = "XS";
        public const string S = "S";
        public const string M = "M";
        public const string L = "L";
        public const string XL = "XL";
        public const string XXL = "XXL";
    }
}
