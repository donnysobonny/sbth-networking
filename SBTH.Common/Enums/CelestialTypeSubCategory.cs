﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SBTH.Common.Enums {
    public class CelestialTypeSubCategory {
        public const string none = "none";
        public const string gas_giant = "gas_giant";
        public const string barren = "barren";
        public const string desert = "desert";
        public const string iron = "iron";
        public const string lava = "lava";
        public const string ocean = "ocean";
        public const string ice = "ice";
        public const string terrestrial = "terrestrial";
        public const string carbon = "carbon";
    }
}
