﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace SBTH.Common.Enums {
    public enum EventCode : byte {

        /// <summary>
        /// an RPC is being sent to you
        /// </summary>
        RPC = 254,

        /// <summary>
        /// A sub server is sending an update to the master server
        /// </summary>
        sendSubToMasterUpdate = 252,

        /// <summary>
        /// a peer has joined the room
        /// </summary>
        joinedRoom = 251,
        /// <summary>
        /// a peer has left the room
        /// </summary>
        leftRoom = 250,

        /// <summary>
        /// A peer has set their player data
        /// </summary>
        playerDataSet = 249,

        /// <summary>
        /// Receiving a list of compiled events
        /// </summary>
        eventList = 248,

        itemAddedToRoom = 247,
        itemRemovedFromRoom = 246,
        realTimeUpdate = 245,
        highPriorityUpdate = 244,
        lowPriorityUpdate = 243
    }
}
