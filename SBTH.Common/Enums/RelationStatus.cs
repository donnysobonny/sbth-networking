﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SBTH.Common.Enums {
    public enum RelationStatus {
        squad,
        colony,
        war,
        criminal,
        neutral
    }
}
