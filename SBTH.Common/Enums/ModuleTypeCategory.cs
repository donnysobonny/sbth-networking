﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SBTH.Common.Enums {
    public static class ModuleTypeCategory {
        public const string engine = "engine";
        public const string shield = "shield";
        public const string hull = "hull";
        public const string utility = "utility";
        public const string weapon = "weapon";
    }
}
