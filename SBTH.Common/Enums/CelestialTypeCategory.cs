﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SBTH.Common.Enums {
    public class CelestialTypeCategory {
        public const string sun = "sun";
        public const string planet = "planet";
        public const string moon = "moon";
    }
}
