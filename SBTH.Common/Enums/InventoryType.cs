﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SBTH.Common.Enums {
    public static class InventoryType {
        public const string personal_cargo = "personal_cargo";
        public const string colony_cargo = "colony_cargo";
        public const string public_cargo = "public_cargo";
        public const string locked_cargo = "locked_cargo";
        public const string hangar = "hangar";
    }
}
