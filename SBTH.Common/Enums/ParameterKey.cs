﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace SBTH.Common.Enums {
    public enum ParameterKey : byte {
        serverLoad = 255,

        roomId = 254,
        playerId = 253,
        colonyId = 252,
        systemId = 251,
        landmarkId = 250,
        inventoryId = 249,

        expireInSeconds = 248,

        serverIP = 247,

        masterPeerId = 246,

        peerId = 245,
        peerTarget = 244,

        itemId = 243,
        itemIds = 242,
        itemTypeId = 241,

        eventList = 239,

        RPCMethod = 238,
        RPCArgs = 237,
        RPCBuffered = 236,

        playerData = 235,

        eventCode = 234,
        operationCode = 233,
        parameters = 232,

        time = 231,

        realTimeData = 229,
        highPriorityData = 228,
        lowPriorityData = 227
    }
}
