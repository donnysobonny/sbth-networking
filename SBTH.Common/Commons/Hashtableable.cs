﻿using System.Collections;

namespace SBTH.Common.Commons {
    public class Hashtableable {

        public virtual Hashtable Compress(Hashtableable old) {
            return new Hashtable();
        }

        public virtual void Populate(Hashtable ht) { }

        public virtual Hashtable Extract() {
            return new Hashtable();
        }
    }
}
