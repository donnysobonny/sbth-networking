﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SBTH.Common.Models {
    [Serializable]
    public class Player {
        public int id;
        public int userId;
        public string name;
        public int colonyId;
        public int squadId;
        public int role;
        public int currentStationId;
        public int currentShipAsPilotId;
        public int currentShipAsPassengerId;
    }
}