﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace SBTH.Common.Models {
    [Serializable]
    public class ItemType {
        public int id;
        public string name;
        public string description;
        public string category;
        public string subCategory;
        public string size;
        public string prefab;
        public float packagedVolume;
        public float unpackagedVolume;
        public string baseStats;
    }
}
