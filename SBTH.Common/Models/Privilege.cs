﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SBTH.Common.Models {
    [Serializable]
    public class Privilege {
        public int id;
        public string category;
        public string name;
        public string description;
    }
}
