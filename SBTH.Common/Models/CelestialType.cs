﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SBTH.Common.Models {
    [Serializable]
    public class CelestialType {
        public int id;
        public string name;
        public string category;
        public string subCategory;
        public string prefab;
    }
}
