﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SBTH.Common.Models {
    [Serializable]
    public class Role {
        public int id;
        public int colonyId;
        public string name;
        public string description;
        public List<int> privileges;
    }
}
