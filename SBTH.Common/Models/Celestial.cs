﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SBTH.Common.Models {
    [Serializable]
    public class Celestial {
        public int id;
        public int solarSystemId;
        public int celestialType;
        public float posX;
        public float posZ;
    }
}
