﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SBTH.Common.Models {
    [Serializable]
    public class Landmark {
        public int id;
        public string name;
        public int solarSystemId;
        public int connectedSolarSystemId;
        public int landmarkType;
        public int celestialId;
        public float posX;
        public float posY;
        public float posZ;
        public bool isFixed;
        public float landingDistance;
    }
}
