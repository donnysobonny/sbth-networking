﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SBTH.Common.Models {
    [Serializable]
    public class Contract {
        public int id;
        public int issueColonyId;
        public int issuePlayerId;
        public string dateIssued;
        public string type;
        public int targetColonyId;
        public int targetItemId;
        public int acceptedColonyId;
        public List<int> acceptedColonyRoles;
        public int acceptedPlayerId;
        public string dateExpired;
    }
}
