﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SBTH.Common.Models {
    [Serializable]
    public class LandmarkType {
        public int id;
        public string name;
        public string description;
        public string category;
        public string subCategory;
        public bool isStatic;
    }
}
