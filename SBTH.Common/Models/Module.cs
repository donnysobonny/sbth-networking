﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SBTH.Common.Models {
    [Serializable]
    public class Module {
        public int id;
        public int moduleType;
        public string finalPassiveStats;
        public string finalActiveStats;
    }
}
