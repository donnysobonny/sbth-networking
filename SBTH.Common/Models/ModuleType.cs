﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SBTH.Common.Models {
    [Serializable]
    public class ModuleType {
        public int id;
        public string name;
        public string description;
        public string category;
        public string subCategory;
        public string shipSize;
        public int power;
        public bool isActive;
        public string passiveStats;
        public string activeStats;
    }
}
