﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SBTH.Common.Models {
    [Serializable]
    public class Criminal {
        public int id;
        public int issueColonyId;
        public string dateIssued;
        public int targetPlayerId;
        public string dateExpired;
    }
}
