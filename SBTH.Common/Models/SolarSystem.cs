﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SBTH.Common.Models {
    [Serializable]
    public class SolarSystem {
        public int id;
        public string name;
        public string solicitation;
        public float posX;
        public float posZ;
        public List<int> connectedSystems;
    }
}
