﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace SBTH.Common.Models {
    [Serializable]
    public class Item {
        public int id;
        public int itemType;
        public string name = "";
        public int count = 1;
        public int playerId = 0;
        public int colonyId = 0;
        public int solarSystemId = 0;
        public int landmarkId = 0;
        public int staticLandmarkId = 0;
        public int inventoryId = 0;
        public int[] weapons;
        public int[] shields;
        public int[] hulls;
        public int[] engines;
        public int[] utilities;
        public string finalStats;
        public string synchronizeData;
        public string updateData;
    }
}
