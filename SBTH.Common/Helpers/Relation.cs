﻿using SBTH.Common.Enums;
using System;

namespace SBTH.Common.Helpers {
    [Serializable]
    public class Relation {
        public int youId = 0;
        public int theyId = 0;
        public bool sameSquad = false;
        public bool sameColony = false;
        public float warExpiry = 0;
        public float theyCriminalExpiry = 0;
        public float youCriminalExpiry = 0;
        public float theyEngageExpiry = 0;
        public float youEngageExpiry = 0;
        public float theyContractExpiry = 0;
        public float youContractExpiry = 0;

        public RelationStatus getYouRelationStatus() {
            if(this.sameSquad) {
                return RelationStatus.squad;
            }
            if(this.sameColony) {
                return RelationStatus.colony;
            }
            if(this.warExpiry > 0) {
                return RelationStatus.war;
            }
            if(this.youCriminalExpiry > 0) {
                return RelationStatus.criminal;
            }
            return RelationStatus.neutral;
        }

        public RelationStatus getTheyRelationStatus() {
            if(this.sameSquad) {
                return RelationStatus.squad;
            }
            if(this.sameColony) {
                return RelationStatus.colony;
            }
            if(this.warExpiry > 0) {
                return RelationStatus.war;
            }
            if(this.theyCriminalExpiry > 0) {
                return RelationStatus.criminal;
            }
            return RelationStatus.neutral;
        }

        public bool youCanEngage() {
            switch(this.getTheyRelationStatus()) {
                case RelationStatus.neutral:
                    return false;
                default:
                    return true;
            }
        }

        public bool theyCanEngage() {
            switch(this.getYouRelationStatus()) {
                case RelationStatus.neutral:
                    return false;
                default:
                    return true;
            }
        }

        public bool youCanBounty() {
            switch(this.getTheyRelationStatus()) {
                case RelationStatus.squad:
                case RelationStatus.colony:
                    return false;
                default:
                    return true;
            }
        }

        public bool theyCanBounty() {
            switch(this.getYouRelationStatus()) {
                case RelationStatus.squad:
                case RelationStatus.colony:
                    return false;
                default:
                    return true;
            }
        }

        public void subtractTime(float time) {
            if(this.warExpiry > 0) {
                this.warExpiry -= time;
                if(this.warExpiry < 0) {
                    this.warExpiry -= 0;
                }
            }
            if(this.theyCriminalExpiry > 0) {
                this.theyCriminalExpiry -= time;
                if(this.theyCriminalExpiry < 0) {
                    this.theyCriminalExpiry -= 0;
                }
            }
            if(this.youCriminalExpiry > 0) {
                this.youCriminalExpiry -= time;
                if(this.youCriminalExpiry < 0) {
                    this.youCriminalExpiry -= 0;
                }
            }
            if(this.theyEngageExpiry > 0) {
                this.theyEngageExpiry -= time;
                if(this.theyEngageExpiry < 0) {
                    this.theyEngageExpiry -= 0;
                }
            }
            if(this.youEngageExpiry > 0) {
                this.youEngageExpiry -= time;
                if(this.youEngageExpiry < 0) {
                    this.youEngageExpiry -= 0;
                }
            }
            if(this.theyContractExpiry > 0) {
                this.theyContractExpiry -= time;
                if(this.theyContractExpiry < 0) {
                    this.theyContractExpiry -= 0;
                }
            }
            if(this.youContractExpiry > 0) {
                this.youContractExpiry -= time;
                if(this.youContractExpiry < 0) {
                    this.youContractExpiry -= 0;
                }
            }
        }
    }
}
