﻿using SBTH.Common.Commons;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace SBTH.Common.Objects {
    public class PlayerData : Hashtableable {

        public int id = 0;

        public override Hashtable Extract() {
            Hashtable ht = new Hashtable();
            ht.Add("id", this.id);

            return ht;
        }

        public override void Populate(Hashtable hashTable) {
            if (hashTable.ContainsKey("id")) {
                this.id = (int)hashTable["id"];
            }
        }
    }
}
