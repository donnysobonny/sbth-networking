﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SBTH.Common.Objects.Stats {
    [Serializable]
    public class ModuleStats {
        public float maxThrottleAdd = 0;
        public float maxThrottleMult = 1;
        public float throttleCorrectionAdd = 0;
        public float throttleCorrectionMult = 1;
        public float maxLateralAdd = 0;
        public float maxLateralMult = 1;
        public float minLateralAdd = 0;
        public float minLateralMult = 1;
        public float lateralCorrectionAdd = 0;
        public float lateralCorrectionMult = 1;
        public float boosterSpeedAdd = 0;
        public float boosterSpeedMult = 1;
        public float maxRotationAdd = 0;
        public float maxRotationMult = 1;
        public float minRotationAdd = 0;
        public float minRotationMult = 1;
    }
}
