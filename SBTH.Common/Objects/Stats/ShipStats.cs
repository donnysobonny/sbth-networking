﻿using SBTH.Common.Commons;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Collections;

namespace SBTH.Common.Objects.Stats {
    [Serializable]
    public class ShipStats {
        public float hullStrength;
        public float shieldStrength;
        public float shieldRegen;

        /// <summary>
        /// The ship's maximum throttle
        /// </summary>
        public float maxThrottle;
        /// <summary>
        /// How quickly the ship can speed up/slow down
        /// </summary>
        public float throttleCorrection;

        /// <summary>
        /// The ship's lateral speed at zero throttle
        /// </summary>
        public float maxLateral;
        /// <summary>
        /// The ship's lateral speed at full throttle + booster
        /// </summary>
        public float minLateral;
        /// <summary>
        /// How quickly the lateral direction can change
        /// </summary>
        public float lateralCorrection;

        /// <summary>
        /// When boosters are activated, this value is added to the maxThrottle as the ship's current speed
        /// </summary>
        public float boosterSpeed;

        /// <summary>
        /// The ship's rotation speed at zero throttle
        /// </summary>
        public float maxRotation;
        /// <summary>
        /// The ship's rotation speed at maxThrottle + booster
        /// </summary>
        public float minRotation;

        public float maxTilt;
        public float cameraYOffset;
        public float cameraMinDist;
        public float cameraMaxDist;
        public float cameraStraffeOffset;

        public int weaponSlots = 0;
        public int engineSlots = 0;
        public int shieldSlots = 0;
        public int hullSlots = 0;
        public int utilitySlots = 0;
    }
}
