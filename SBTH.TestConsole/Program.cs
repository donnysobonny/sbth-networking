﻿using ExitGames.Client.Photon;
using System;
using System.Management;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using SBTH.Common.Enums;
using System.Collections;

namespace SBTH.TestConsole {
    public class Program {
        static void Main() {
            new Program();
        }

        private PhotonPeer masterPeer;
        private PhotonPeer subPeer;

        private bool connectedToMaster = false;
        private bool connectedToSub = false;
        private bool inRoom = false;

        public Program() {

            Console.WriteLine("Program started");

            this.masterPeer = new PhotonPeer(new MasterPeerListener(this), ConnectionProtocol.Udp);      
            this.subPeer = new PhotonPeer(new SubPeerListener(this), ConnectionProtocol.Udp);
            //set the channel count to 6
            this.masterPeer.ChannelCount = this.subPeer.ChannelCount = (byte)6;

            //connect to the master server
            Console.WriteLine(this.masterPeer.Connect("localhost:5055", "SBTH.MasterServer"));

            while (!Console.KeyAvailable) {
                if (this.masterPeer != null) this.masterPeer.Service();
                if (this.subPeer != null) this.subPeer.Service();

                Thread.Sleep(100);
            }
            if (this.masterPeer != null) this.masterPeer.Disconnect();
            if (this.subPeer != null) this.subPeer.Disconnect();

            
        }

        public void MasterDebugReturn(DebugLevel level, string message) {
            //throw new NotImplementedException();
        }

        public void MasterOnEvent(EventData eventData) {
            //throw new NotImplementedException();
        }

        public void MasterOnOperationResponse(OperationResponse operationResponse) {
            Console.WriteLine("operation response from master: " + operationResponse.ToStringFull());

            switch(operationResponse.OperationCode) {
                case (byte)OperationCode.peerRequstSubBySystem:
                    Console.WriteLine("master server says to go to the server located at " + operationResponse.Parameters[(byte)ParameterKey.serverIP]);
                    //connect to the sub server
                    this.subPeer.Connect((string)operationResponse.Parameters[(byte)ParameterKey.serverIP] + ":5056", "SBTH.SubServer");
                    break;
            }
        }

        public void MasterOnStatusChanged(StatusCode statusCode) {
            Console.WriteLine("Master status changed: " + statusCode);
            switch (statusCode) {
                case StatusCode.Connect:
                    this.connectedToMaster = true;

                    //request to join system 2

                    Dictionary<byte, object> p = new Dictionary<byte, object>();
                    p.Add((byte)ParameterKey.systemId, 2);
                    this.masterPeer.OpCustom((byte)OperationCode.peerRequstSubBySystem, p, true);

                    break;
                default:
                    this.connectedToMaster = false;
                    break;
            }
        }

        public void SubDebugReturn(DebugLevel level, string message) {
            //throw new NotImplementedException();
        }

        public void SubOnEvent(EventData eventData) {
            Console.WriteLine("Sub event received: " + eventData.ToStringFull());
        }

        public void SubOnOperationResponse(OperationResponse operationResponse) {
            Console.WriteLine("Sub operation response: " + operationResponse.ToStringFull());

            switch(operationResponse.OperationCode) {
                case (byte)OperationCode.createOrJoinRoom:
                    //success?
                    if(operationResponse.ReturnCode == 0) {
                        Console.WriteLine("Joined room successfully!");
                        this.inRoom = true;                                              
                    }
                    break;
            }
        }

        public void SubOnStatusChanged(StatusCode statusCode) {
            Console.WriteLine("Sub status changed: " + statusCode);
            switch (statusCode) {
                case StatusCode.Connect:
                    this.connectedToSub = true;
                    //disconnect from master
                    this.masterPeer.Disconnect();
                    Console.WriteLine("We have connected to the sub server!!");

                    //join the room
                    Dictionary<byte, object> p = new Dictionary<byte, object>();
                    Hashtable ht = new Hashtable();
                    ht.Add("id", 100);
                    p.Add((byte)ParameterKey.roomId, 1);
                    p.Add((byte)ParameterKey.playerData, ht);
                    this.subPeer.OpCustom((byte)OperationCode.createOrJoinRoom, p, true);

                    break;
                default:
                    this.connectedToSub = false;
                    break;
            }
        }
    }
}
