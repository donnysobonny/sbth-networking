﻿using ExitGames.Client.Photon;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBTH.TestConsole {
    public class MasterPeerListener : IPhotonPeerListener {

        private Program program;

        public MasterPeerListener(Program program) {
            this.program = program;
        }

        public void DebugReturn(DebugLevel level, string message) {
            program.MasterDebugReturn(level, message);
        }

        public void OnEvent(EventData eventData) {
            program.MasterOnEvent(eventData);
        }

        public void OnOperationResponse(OperationResponse operationResponse) {
            program.MasterOnOperationResponse(operationResponse);
        }

        public void OnStatusChanged(StatusCode statusCode) {
            program.MasterOnStatusChanged(statusCode);
        }
    }
}
