﻿using ExitGames.Client.Photon;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBTH.TestConsole {
    public class SubPeerListener : IPhotonPeerListener {

        private Program program;

        public SubPeerListener(Program program) {
            this.program = program;
        }

        public void DebugReturn(DebugLevel level, string message) {
            program.SubDebugReturn(level, message);
        }

        public void OnEvent(EventData eventData) {
            program.SubOnEvent(eventData);
        }

        public void OnOperationResponse(OperationResponse operationResponse) {
            program.SubOnOperationResponse(operationResponse);
        }

        public void OnStatusChanged(StatusCode statusCode) {
            program.SubOnStatusChanged(statusCode);
        }
    }
}
