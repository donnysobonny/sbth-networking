﻿using ExitGames.Logging;
using ExitGames.Logging.Log4Net;
using log4net.Config;
using Photon.SocketServer;
using Photon.SocketServer.ServerToServer;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Npgsql;
using SBTH.Server;
using ExtorioApi;
using LitJson;
using System.Collections;
using ExitGames.Concurrency.Fibers;
using System.Net.Http;
using SBTH.Common.Enums;
using SBTH.SubServer.Objects.CustomTypes;

namespace SBTH.SubServer {
    public class Application : ApplicationBase {
        private readonly ILogger log = LogManager.GetCurrentClassLogger();

        public PoolFiber fiber = new PoolFiber();

        public ServerPeer masterServer { get; private set; }

        /// <summary>
        /// We store a list of rooms that are live on the sub server here. Any time that we want to add/remove rooms from this array, we must lock the array itself.
        /// </summary>
        public Dictionary<int, Room> rooms = new Dictionary<int, Room>();

        /// <summary>
        /// We store a reference to a peer and it's current room location here, so that we can locate the peer's room server-wide. This should be locked when modified.
        /// </summary>
        public Dictionary<int, int> peerToRoom = new Dictionary<int, int>();

        public static Application get() {
            return (Application)Photon.SocketServer.ApplicationBase.Instance;
        }

        protected override ServerPeerBase CreateServerPeer(InitResponse initResponse, object state) {
            // creating master server peer
            return this.masterServer = new ServerPeer(initResponse.Protocol, initResponse.PhotonPeer);
        }

        protected override PeerBase CreatePeer(InitRequest initRequest) {
            // creating the player peer
            return new PlayerPeer(initRequest);
        }

        protected override void Setup() {
            log4net.GlobalContext.Properties["Photon:ApplicationLogPath"] = Path.Combine(this.ApplicationRootPath, "log");

            // log4net
            string path = Path.Combine(this.BinaryPath, "log4net.config");
            var file = new FileInfo(path);
            if (file.Exists) {
                LogManager.SetLoggerFactory(Log4NetLoggerFactory.Instance);
                XmlConfigurator.ConfigureAndWatch(file);
            }

            log.InfoFormat("Created master server application: type={0}", Instance.GetType());

            //! on the server, we do not need to use the api. In stead, it makes more sense to go straight into the db
            ////create the extorio connection
            //this.extorioConnection = new Connection(ServerSettings.extorio_domain);
            ////login
            //this.extorioConnection.loginAsync(delegate (string session) {
            //    log.DebugFormat("Logged into extorio with session: {0}", session);
            //}, "subserver", "194shibby1", delegate (string error) {
            //    log.DebugFormat("Login error: {0}", error);
            //});

            //connect to the master server
            IPAddress ip;
            IPAddress.TryParse("127.0.0.1", out ip);
            IPEndPoint enpoint = new IPEndPoint(ip, 4520);
            this.ConnectToServerTcp(enpoint, "SBTH.MasterServer", enpoint);

            Protocol.TryRegisterCustomType(typeof(_Vector3), (byte)CustomTypeCodes._vector3, _Vector3.Serialize, _Vector3.Deserialize);
            Protocol.TryRegisterCustomType(typeof(_Quaternion), (byte)CustomTypeCodes._quaternion, _Quaternion.Serialize, _Quaternion.Deserialize);
        }

        public NpgsqlConnection db() {
            return new NpgsqlConnection("Host=" + ServerSettings.db_host + ";Username=" + ServerSettings.db_user + ";Password=" + ServerSettings.db_pass + ";Database=" + ServerSettings.db_name + "");
        }

        protected override void TearDown() {

        }
    }
}
