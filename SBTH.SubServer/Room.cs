﻿using ExitGames.Concurrency.Fibers;
using ExitGames.Logging;
using Npgsql;
using Photon.SocketServer;
using SBTH.Common;
using SBTH.Common.Enums;
using SBTH.Common.Objects;
using SBTH.Server;
using SBTH.SubServer.Helpers;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LitJson;

namespace SBTH.SubServer {
    public class Room {

        public ILogger log = LogManager.GetCurrentClassLogger();

        public int roomId { get; private set; }

        /// <summary>
        /// Whenever we want to modify the data related to peers, we must lock this key
        /// </summary>
        public readonly object peerKey = new object();
        /// <summary>
        /// Whenever we want to modify the data related to stored events, we must lock this key
        /// </summary>
        public readonly object eventKey = new object();
        /// <summary>
        /// Whenever we want to modify data related to items, we must lock this key
        /// </summary>
        public readonly object itemKey = new object();
        /// <summary>
        /// Whenever we want to modify the real-time data, we must lock this key
        /// </summary>
        public readonly object realTimeKey = new object();
        /// <summary>
        /// Whenever we want to modify the low priority data, we must lock this key
        /// </summary>
        public readonly object lowPriorityKey = new object();
        /// <summary>
        /// Whenever we want to modify the high priority data, we must lock this key
        /// </summary>
        public readonly object highPriorityKey = new object();

        /// <summary>
        /// The list of player peers in this room
        /// </summary>
        public Dictionary<int, PlayerPeer> playerPeers = new Dictionary<int, PlayerPeer>();
        /// <summary>
        /// The currently set master peer
        /// </summary>
        public int masterPeerId = 0;

        public List<int> items = new List<int>();

        #region event properties
        /// <summary>
        /// The list of stored events in the room
        /// </summary>
        public Dictionary<int, Dictionary<byte, object>> storedEvents = new Dictionary<int, Dictionary<byte, object>>();
        /// <summary>
        /// A reference of peer => events to reference which peer owns which events. Useful for clearing up peer-owned events.
        /// </summary>
        public Dictionary<int, List<int>> peerEvents = new Dictionary<int, List<int>>();
        /// <summary>
        /// A reference of serialized item => events to reference which object owns which event. Useful for clearing up object events.
        /// </summary>
        public Dictionary<int, List<int>> itemEvents = new Dictionary<int, List<int>>();
        /// <summary>
        /// An index used to create the uids for events
        /// </summary>
        public int storedEventIndex = 0;
        #endregion

        /// <summary>
        /// Similar to a peer's request fiber, we use this fiber to queue up actions that are room-specific, in the attempt to make rooms more independent from each other
        /// </summary>
        public PoolFiber fiber = new PoolFiber();

        public Dictionary<int, Hashtable> realTimeData = new Dictionary<int, Hashtable>();
        public Dictionary<int, Dictionary<string, float>> realTimeDataUpdateTimes = new Dictionary<int, Dictionary<string, float>>();

        public Dictionary<int, Hashtable> highPriorityData = new Dictionary<int, Hashtable>();
        public Dictionary<int, Dictionary<string, float>> highPriorityDataUpdateTimes = new Dictionary<int, Dictionary<string, float>>();

        public Dictionary<int, Hashtable> lowPriorityData = new Dictionary<int, Hashtable>();
        public Dictionary<int, Dictionary<string, float>> lowPriorityDataUpdateTimes = new Dictionary<int, Dictionary<string, float>>();
        public Dictionary<int, Hashtable> lowPriorityDataQueue = new Dictionary<int, Hashtable>();

        public Room(int roomId) {
            this.roomId = roomId;
            //get the item list
            Application app = Application.get();
            NpgsqlConnection conn = app.db();
            conn.Open();
            NpgsqlCommand c = new NpgsqlCommand("SELECT id, \"lowPriorityData\", \"highPriorityData\" FROM sbth_classes_models_item WHERE \"landmarkId\" = @1 OR \"staticLandmarkId\" = @1", conn);
            c.Parameters.Add(new NpgsqlParameter("@1", this.roomId));
            NpgsqlDataReader r = c.ExecuteReader();
            lock(this.itemKey) {
                while(r.Read()) {
                    int itemId = Convert.ToInt32(r["id"]);
                    this.items.Add(itemId);
                    this.itemEvents[itemId] = new List<int>();
                    this.realTimeData[itemId] = new Hashtable();
                    this.realTimeDataUpdateTimes[itemId] = new Dictionary<string, float>();

                    this.highPriorityData[itemId] = JsonMapper.ToObject<Hashtable>((string)r["highPriorityData"]);
                    this.highPriorityDataUpdateTimes[itemId] = new Dictionary<string, float>();

                    this.lowPriorityData[itemId] = JsonMapper.ToObject<Hashtable>((string)r["lowPriorityData"]);
                    this.lowPriorityDataUpdateTimes[itemId] = new Dictionary<string, float>();
                }
            }
            conn.Close();

            fiber.Start();
            //schedule the update loop on an interval
            this.fiber.ScheduleOnInterval(this.Update, 0, (long)(CommonSettings.low_priority_update_interval * 1000));
        }

        private void Update() {
            this.sendLowPriorityUpdates();
        }

        private void sendLowPriorityUpdates() {

            Dictionary<int, Hashtable> updates;

            lock(this.lowPriorityKey) {
                updates = new Dictionary<int, Hashtable>(this.lowPriorityDataQueue);
                this.lowPriorityDataQueue.Clear();
            }

            if(updates.Count > 0) {
                //send updates
                float time = float.Parse(DateTime.Now.TimeOfDay.TotalSeconds.ToString());
                Dictionary<byte, object> p = new Dictionary<byte, object>();
                p.Add((byte)ParameterKey.lowPriorityData, updates);
                p.Add((byte)ParameterKey.time, time);
                new PlayerEvent(
                    new EventData() {
                        Code = (byte)EventCode.lowPriorityUpdate,
                        Parameters = p
                    },
                    new SendParameters() {
                        ChannelId = (byte)Channel.lowPriorityUpdates,
                        Unreliable = true
                    }
                ).broadcast(this.getPlayers());
            }
        }

        public void handleStoredEvents(PlayerPeer targetPeer) {
            if (this.storedEvents.Count > 0) {
                //we need to convert the Dictionary<int, Dictionary<byte, object>> of storedEvents to a list in order to be properly serialized
                List<Dictionary<byte, object>> compressedEvents = new List<Dictionary<byte, object>>();
                Dictionary<int, Dictionary<byte, object>> _events = new Dictionary<int, Dictionary<byte, object>>(this.storedEvents);
                foreach (KeyValuePair<int, Dictionary<byte, object>> pair in _events) {
                    compressedEvents.Add(pair.Value);
                }
                Dictionary<byte, object> p = new Dictionary<byte, object>();
                p.Add((byte)ParameterKey.eventList, compressedEvents);
                new PlayerEvent(new EventData() {
                    Code = (byte)EventCode.eventList,
                    Parameters = p
                }, new SendParameters() {
                    Unreliable = false,
                    ChannelId = (byte)Channel.important
                }).handle(targetPeer);
            }            
        }

        public void logPlayerPeers() {
            log.DebugFormat("Logging player peers for room ", this.roomId);
            Dictionary<int, PlayerPeer> ps = new Dictionary<int, PlayerPeer>(this.playerPeers);
            foreach (KeyValuePair<int, PlayerPeer> pair in ps) {
                log.DebugFormat(" -> found peer id={0}", pair.Key);
            }
        }

        public void logStoredEvents() {
            log.DebugFormat("Logging stored events for room ", this.roomId);
            Dictionary<int, Dictionary<byte, object>> es = new Dictionary<int, Dictionary<byte, object>>(this.storedEvents);
            foreach (KeyValuePair<int, Dictionary<byte, object>> pair in es) {
                log.DebugFormat(" -> event found id={0}", pair.Key);
            }
        }

        public void logPlayerEvents() {
            log.DebugFormat("Logging player events for room ", this.roomId);
            Dictionary<int, List<int>> pes = new Dictionary<int, List<int>>(this.peerEvents);
            foreach (KeyValuePair<int, List<int>> pair in pes) {
                log.DebugFormat(" -> The peer {0} has stored events:", pair.Key);
                pair.Value.ForEach(delegate (int id) {
                    log.DebugFormat("  -> {0}", id);
                });
            }
        }

        public Dictionary<int, PlayerPeer> getPlayers(PeerTarget peerTarget = PeerTarget.all, int myPeerId = 0, int targetPeerId = 0) {
            Dictionary<int, PlayerPeer> peers;
            switch (peerTarget) {
                case PeerTarget.all:
                    return new Dictionary<int, PlayerPeer>(this.playerPeers);
                case PeerTarget.other:
                    peers = new Dictionary<int, PlayerPeer>(this.playerPeers);
                    peers.Remove(myPeerId);
                    return peers;
                case PeerTarget.one:
                    peers = new Dictionary<int, PlayerPeer>();
                    if (this.playerPeers.ContainsKey(targetPeerId)) {
                        peers.Add(targetPeerId, this.playerPeers[targetPeerId]);
                    }
                    return peers;
                case PeerTarget.self:
                    peers = new Dictionary<int, PlayerPeer>();
                    if (this.playerPeers.ContainsKey(myPeerId)) {
                        peers.Add(myPeerId, this.playerPeers[myPeerId]);
                    }
                    return peers;
                default:
                    return new Dictionary<int, PlayerPeer>(this.playerPeers);
            }
        }
    }
}
