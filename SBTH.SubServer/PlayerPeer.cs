﻿using Photon.SocketServer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PhotonHostRuntimeInterfaces;
using ExitGames.Logging;
using SBTH.SubServer.Helpers;
using SBTH.Common.Enums;
using SBTH.Common.Objects;

namespace SBTH.SubServer {
    public class PlayerPeer : PeerBase {

        private ILogger log = LogManager.GetCurrentClassLogger();

        public PlayerData data = new PlayerData();

        public PlayerPeer(InitRequest initRequest)
            : base(initRequest.Protocol, initRequest.PhotonPeer) {

        }

        protected override void OnDisconnect(DisconnectReason reasonCode, string reasonDetail) {
            //by default all disconnects will be actioned with the "leaveRoom" operation
            //which removes the peer and their events, and leaves their ship in space
            //unless of course the peer has already called leaveRoomAndSpace (or leaveSpace), in which case their ship is safe already
            log.DebugFormat("Peer {0} has disconnected. {1} : {2}", this.ConnectionId, reasonCode, reasonDetail);

            Dictionary<byte, object> p = new Dictionary<byte, object>();
            p.Add((byte)ParameterKey.peerId, this.ConnectionId);
            new PlayerOperation(this, new OperationRequest() {
                OperationCode = (byte)OperationCode.leaveRoom,
                Parameters = p
            }, new SendParameters() {
                ChannelId = (byte)Channel.general
            }).handle();
        }

        protected override void OnOperationRequest(OperationRequest operationRequest, SendParameters sendParameters) {
            log.DebugFormat("Operation request recieved from player peer {0}: {1}", this.ConnectionId, operationRequest.OperationCode);

            //handle the operation
            new PlayerOperation(this, operationRequest, sendParameters).handle();
        }
    }
}
