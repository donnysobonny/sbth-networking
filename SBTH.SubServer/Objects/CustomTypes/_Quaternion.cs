﻿using ExitGames.Client.Photon;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBTH.SubServer.Objects.CustomTypes {
    public class _Quaternion {
        public float x;
        public float y;
        public float z;
        public float w;

        public static object Deserialize(byte[] data) {
            _Quaternion q = new _Quaternion();
            int index = 0;
            Protocol.Deserialize(out q.x, data, ref index);
            Protocol.Deserialize(out q.y, data, ref index);
            Protocol.Deserialize(out q.z, data, ref index);
            Protocol.Deserialize(out q.w, data, ref index);
            return q;
        }

        public static byte[] Serialize(object customType) {
            _Quaternion q = (_Quaternion)customType;
            byte[] bytes = new byte[4 * 4];
            int index = 0;
            Protocol.Serialize(q.x, bytes, ref index);
            Protocol.Serialize(q.y, bytes, ref index);
            Protocol.Serialize(q.z, bytes, ref index);
            Protocol.Serialize(q.w, bytes, ref index);
            return bytes;
        }
    }
}
