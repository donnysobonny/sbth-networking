﻿using ExitGames.Client.Photon;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SBTH.SubServer.Objects.CustomTypes {
    public class _Vector3 {
        public float x;
        public float y;
        public float z;

        public static object Deserialize(byte[] data) {
            _Vector3 v = new _Vector3();
            int index = 0;
            Protocol.Deserialize(out v.x, data, ref index);
            Protocol.Deserialize(out v.y, data, ref index);
            Protocol.Deserialize(out v.z, data, ref index);
            return v;
        }

        public static byte[] Serialize(object customType) {
            _Vector3 v = (_Vector3)customType;
            byte[] bytes = new byte[3 * 4];
            int index = 0;
            Protocol.Serialize(v.x, bytes, ref index);
            Protocol.Serialize(v.y, bytes, ref index);
            Protocol.Serialize(v.z, bytes, ref index);
            return bytes;
        }
    }
}
