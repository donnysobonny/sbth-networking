﻿using Photon.SocketServer.ServerToServer;
using System;
using System.Collections.Generic;
using System.Management;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Photon.SocketServer;
using PhotonHostRuntimeInterfaces;
using SBTH.SubServer.Helpers;
using SBTH.Common.Enums;

namespace SBTH.SubServer {
    public class ServerPeer : ServerPeerBase {

        public ServerPeer(IRpcProtocol protocol, IPhotonPeer nativePeer)
            : base(protocol, nativePeer)
        {
            //schedule the update to the master server on an interval of 2 seconds
            this.RequestFiber.ScheduleOnInterval(() => this.sendMasterServerUpdate(), 1000, 2000);
        }

        protected override void OnDisconnect(DisconnectReason reasonCode, string reasonDetail) {
            throw new NotImplementedException();
        }

        protected override void OnEvent(IEventData eventData, SendParameters sendParameters) {
            throw new NotImplementedException();
        }

        protected override void OnOperationRequest(OperationRequest operationRequest, SendParameters sendParameters) {
            throw new NotImplementedException();
        }

        protected override void OnOperationResponse(OperationResponse operationResponse, SendParameters sendParameters) {
            throw new NotImplementedException();
        }

        protected void sendMasterServerUpdate() {
            if (this.Connected) {

                //get the processor time via WMI
                ManagementObjectSearcher searcher = new ManagementObjectSearcher("select PercentProcessorTime from Win32_PerfFormattedData_PerfOS_Processor WHERE Name = '_Total'");
                int processorTime = 0;
                foreach(ManagementObject obj in searcher.Get()) {
                    processorTime = Convert.ToInt32(obj["PercentProcessorTime"]);
                }

                ServerLoad load;
                //work out the load as a ServerLoad
                if(
                    processorTime >= 0 &&
                    processorTime <= 22
                ) {
                    load = ServerLoad.minimal;
                } else if(
                    processorTime > 22 &&
                    processorTime <= 44
                ) {
                    load = ServerLoad.low;
                } else if(
                    processorTime > 44 &&
                    processorTime <= 66
                ) {
                    load = ServerLoad.moderate;
                } else if(
                    processorTime > 66 &&
                    processorTime <= 88
                ) {
                    load = ServerLoad.high;
                } else {
                    load = ServerLoad.excessive;
                }

                Dictionary<byte, object> p = new Dictionary<byte, object>();
                p.Add((byte)ParameterKey.serverLoad, load);
                this.SendEvent(new EventData() {
                    Code = (byte)EventCode.sendSubToMasterUpdate,
                    Parameters = p
                }, new SendParameters() {
                    ChannelId = (byte)Channel.system
                });
            }
        }
    }
}
