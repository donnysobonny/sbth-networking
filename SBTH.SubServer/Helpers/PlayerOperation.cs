﻿using ExitGames.Logging;
using LitJson;
using Npgsql;
using Photon.SocketServer;
using SBTH.Common.Enums;
using SBTH.Server.Helpers;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBTH.SubServer.Helpers {
    class PlayerOperation : OperationBase {

        private readonly ILogger log = LogManager.GetCurrentClassLogger();

        private new PlayerPeer peer;

        public PlayerOperation(PlayerPeer peer, OperationRequest operationRequest, SendParameters sendParameters) : base((PeerBase)peer, operationRequest, sendParameters) {
            this.peer = peer;
        }

        public override void handle() {
            try {
                switch (operationRequest.OperationCode) {

                    case (byte)OperationCode.createOrJoinRoom:
                        this.createOrJoinRoom();
                        break;
                    case (byte)OperationCode.leaveRoom:
                        this.leaveRoom();
                        break;

                    case (byte)OperationCode.setPlayerData:
                        this.setPlayerData();
                        break;

                    case (byte)OperationCode.RPC:
                        this.RPC();
                        break;

                    case (byte)OperationCode.addItemToRoom:
                        this.addItemToRoom();
                        break;

                    case (byte)OperationCode.removeItemFromRoom:
                        this.removeItemFromRoom();
                        break;

                    case (byte)OperationCode.realTimeUpdate:
                        this.realTimeUpdate();
                        break;
                    case (byte)OperationCode.highPriorityUpdate:
                        this.highPriorityUpdate();
                        break;
                    case (byte)OperationCode.lowPriorityUpdate:
                        this.lowPriorityUpdate();
                        break;

                    default:
                        //unrecognised request
                        this.peer.SendOperationResponse(new OperationResponse() {
                            OperationCode = operationRequest.OperationCode,
                            ReturnCode = (short)ReturnCode.OP_NOT_RECOGNISED,
                            DebugMessage = "Operation not recognised"
                        }, sendParameters);
                        break;
                }
            } catch (Exception ex) {
                //auto send error
                this.peer.SendOperationResponse(new OperationResponse() {
                    OperationCode = operationRequest.OperationCode,
                    ReturnCode = (short)ReturnCode.GENERAL_ERROR,
                    DebugMessage = ex.ToString()
                }, sendParameters);
            }
        }

        private void realTimeUpdate() {
            //item id required
            if(!this.parameterExists(ParameterKey.itemId)) {
                throw new Exception("Item id is required");
            }
            int itemId = this.getParameter<int>(ParameterKey.itemId);
            //realTimeData required
            if(!this.parameterExists(ParameterKey.realTimeData)) {
                throw new Exception("Real time data required");
            }
            Hashtable realTimeData = this.getParameter<Hashtable>(ParameterKey.realTimeData);
            //time required
            if(!this.parameterExists(ParameterKey.time)) {
                throw new Exception("Time required");
            }
            float time = this.getParameter<float>(ParameterKey.time);

            Application app = Application.get();
            //if the player is in a room
            lock (app.peerToRoom) {
                if(app.peerToRoom.ContainsKey(this.peer.ConnectionId)) {
                    int roomId = app.peerToRoom[this.peer.ConnectionId];
                    lock (app.rooms) {
                        if(app.rooms.ContainsKey(roomId)) {
                            Room room = app.rooms[roomId];

                            bool foundUpdate = false;
                            Hashtable compressedUpdate = new Hashtable();

                            lock(room.itemKey) {
                                if(room.items.Contains(itemId)) {
                                    lock(room.realTimeKey) {
                                        Dictionary<string, float> currentUpdateTimes = room.realTimeDataUpdateTimes[itemId];
                                        Hashtable currentRealTimeData = room.realTimeData[itemId];

                                        foreach(DictionaryEntry pair in realTimeData) {
                                            string key = pair.Key.ToString();
                                            //make sure each property is the most recent one
                                            if(!currentUpdateTimes.ContainsKey(key)) {
                                                currentUpdateTimes[key] = time;
                                            }
                                            if(time >= currentUpdateTimes[key]) {
                                                foundUpdate = true;
                                                currentRealTimeData[key] = pair.Value;
                                                compressedUpdate[key] = pair.Value;
                                                currentUpdateTimes[key] = time;
                                            }
                                        }
                                        if(foundUpdate) {
                                            room.realTimeDataUpdateTimes[itemId] = currentUpdateTimes;
                                            room.realTimeData[itemId] = currentRealTimeData;
                                        }
                                    }
                                }
                            }

                            if(foundUpdate) {
                                //create an event to notify other peers
                                Dictionary<byte, object> p = new Dictionary<byte, object>();
                                p.Add((byte)ParameterKey.itemId, itemId);
                                p.Add((byte)ParameterKey.realTimeData, compressedUpdate);
                                p.Add((byte)ParameterKey.time, time);
                                new PlayerEvent(
                                    new EventData() {
                                        Code = (byte)EventCode.realTimeUpdate,
                                        Parameters = p
                                    }, this.sendParameters
                                ).broadcast(room.getPlayers(PeerTarget.other,this.peer.ConnectionId));
                            }
                        }
                    }
                }
            }
        }

        private void highPriorityUpdate() {
            //item id required
            if(!this.parameterExists(ParameterKey.itemId)) {
                throw new Exception("Item id is required");
            }
            int itemId = this.getParameter<int>(ParameterKey.itemId);
            //high priority data required
            if(!this.parameterExists(ParameterKey.highPriorityData)) {
                throw new Exception("High priority data required");
            }
            Hashtable highPriorityData = this.getParameter<Hashtable>(ParameterKey.highPriorityData);
            //time required
            if(!this.parameterExists(ParameterKey.time)) {
                throw new Exception("Time required");
            }
            float time = this.getParameter<float>(ParameterKey.time);

            Application app = Application.get();
            //if the player is in a room
            lock (app.peerToRoom) {
                if(app.peerToRoom.ContainsKey(this.peer.ConnectionId)) {
                    int roomId = app.peerToRoom[this.peer.ConnectionId];
                    lock (app.rooms) {
                        if(app.rooms.ContainsKey(roomId)) {
                            Room room = app.rooms[roomId];

                            bool foundUpdate = false;
                            Hashtable compressedUpdate = new Hashtable();

                            lock (room.itemKey) {
                                if(room.items.Contains(itemId)) {
                                    lock (room.highPriorityKey) {
                                        Dictionary<string, float> hpUpdateTimes = room.highPriorityDataUpdateTimes[itemId];
                                        Hashtable hpData = room.highPriorityData[itemId];

                                        foreach(DictionaryEntry pair in highPriorityData) {
                                            string key = pair.Key.ToString();
                                            //make sure each property is the most recent one
                                            if(!hpUpdateTimes.ContainsKey(key)) {
                                                hpUpdateTimes[key] = time;
                                            }
                                            if(time >= hpUpdateTimes[key]) {
                                                foundUpdate = true;
                                                hpData[key] = pair.Value;
                                                compressedUpdate[key] = pair.Value;
                                                hpUpdateTimes[key] = time;
                                            }
                                        }
                                        if(foundUpdate) {
                                            room.realTimeData[itemId] = hpData;
                                            room.realTimeDataUpdateTimes[itemId] = hpUpdateTimes;
                                        }
                                    }
                                }
                            }

                            if(foundUpdate) {
                                //create an event to notify other peers
                                Dictionary<byte, object> p = new Dictionary<byte, object>();
                                p.Add((byte)ParameterKey.itemId, itemId);
                                p.Add((byte)ParameterKey.highPriorityData, compressedUpdate);
                                p.Add((byte)ParameterKey.time, time);
                                new PlayerEvent(
                                    new EventData() {
                                        Code = (byte)EventCode.highPriorityUpdate,
                                        Parameters = p
                                    }, this.sendParameters
                                ).broadcast(room.getPlayers(PeerTarget.other, this.peer.ConnectionId));
                            }
                        }
                    }
                }
            }
        }

        private void lowPriorityUpdate() {
            //item id required
            if(!this.parameterExists(ParameterKey.itemId)) {
                throw new Exception("Item id is required");
            }
            int itemId = this.getParameter<int>(ParameterKey.itemId);
            //low priority data required
            if(!this.parameterExists(ParameterKey.lowPriorityData)) {
                throw new Exception("High priority data required");
            }
            Hashtable lowPriorityData = this.getParameter<Hashtable>(ParameterKey.lowPriorityData);
            //time required
            if(!this.parameterExists(ParameterKey.time)) {
                throw new Exception("Time required");
            }
            float time = this.getParameter<float>(ParameterKey.time);

            Application app = Application.get();
            //if the player is in a room
            lock (app.peerToRoom) {
                if(app.peerToRoom.ContainsKey(this.peer.ConnectionId)) {
                    int roomId = app.peerToRoom[this.peer.ConnectionId];
                    lock (app.rooms) {
                        if(app.rooms.ContainsKey(roomId)) {
                            Room room = app.rooms[roomId];

                            bool foundUpdate = false;
                            Hashtable compressedUpdate = new Hashtable();

                            lock (room.itemKey) {
                                if(room.items.Contains(itemId)) {
                                    lock (room.lowPriorityKey) {
                                        Dictionary<string, float> lpUpdateTimes = room.lowPriorityDataUpdateTimes[itemId];
                                        Hashtable lpData = room.lowPriorityData[itemId];

                                        foreach(DictionaryEntry pair in lowPriorityData) {
                                            string key = pair.Key.ToString();
                                            //make sure each property is the most recent one
                                            if(!lpUpdateTimes.ContainsKey(key)) {
                                                lpUpdateTimes[key] = time;
                                            }
                                            if(time >= lpUpdateTimes[key]) {
                                                foundUpdate = true;
                                                lpData[key] = pair.Value;
                                                compressedUpdate[key] = pair.Value;
                                                lpUpdateTimes[key] = time;
                                            }
                                        }
                                        if(foundUpdate) {
                                            room.lowPriorityData[itemId] = lpData;
                                            room.lowPriorityDataUpdateTimes[itemId] = lpUpdateTimes;

                                            if(!room.lowPriorityDataQueue.ContainsKey(itemId)) {
                                                room.lowPriorityDataQueue[itemId] = new Hashtable();
                                            }
                                            Hashtable lpDataQueue = room.lowPriorityDataQueue[itemId];
                                            foreach(DictionaryEntry pair in compressedUpdate) {
                                                lpDataQueue[pair.Key] = pair.Value;
                                            }
                                            room.lowPriorityDataQueue[itemId] = lpDataQueue;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        private void addItemToRoom() {
            //item id required
            if(!this.parameterExists(ParameterKey.itemId)) {
                throw new Exception("Item id required");
            }
            int itemId = this.getParameter<int>(ParameterKey.itemId);
            //optionally include realTimeData here
            Hashtable realTimeData = new Hashtable();
            if(this.parameterExists(ParameterKey.realTimeData)) {
                realTimeData = this.getParameter<Hashtable>(ParameterKey.realTimeData);
            }
            //optionally include low prio data
            Hashtable lpData = new Hashtable();
            if(this.parameterExists(ParameterKey.lowPriorityData)) {
                lpData = this.getParameter<Hashtable>(ParameterKey.lowPriorityData);
            }
            //optionally include high prio data
            Hashtable hpData = new Hashtable();
            if(this.parameterExists(ParameterKey.highPriorityData)) {
                hpData = this.getParameter<Hashtable>(ParameterKey.highPriorityData);
            }

            Application app = Application.get();
            //if the player is in a room
            lock (app.peerToRoom) {
                if(app.peerToRoom.ContainsKey(this.peer.ConnectionId)) {
                    int roomId = app.peerToRoom[this.peer.ConnectionId];
                    lock (app.rooms) {
                        if(app.rooms.ContainsKey(roomId)) {
                            Room room = app.rooms[roomId];

                            bool added = false;

                            lock(room.itemKey) {
                                if(!room.items.Contains(itemId)) {
                                    added = true;
                                    room.items.Add(itemId);

                                    string sql = "";
                                    if(lpData.Count == 0 || hpData.Count == 0) {
                                        NpgsqlConnection conn = app.db();
                                        conn.Open();
                                        NpgsqlCommand c = new NpgsqlCommand("SELECT \"lowPriorityData\", \"highPriorityData\" FROM sbth_classes_models_item WHERE id = @1 LIMIT 1", conn);
                                        c.Parameters.Add(new NpgsqlParameter("@1", itemId));
                                        NpgsqlDataReader r = c.ExecuteReader();
                                        while(r.Read()) {
                                            if(lpData.Count == 0) {
                                                lpData = JsonMapper.ToObject<Hashtable>((string)r["lowPriorityData"]);
                                            }
                                            if(hpData.Count == 0) {
                                                hpData = JsonMapper.ToObject<Hashtable>((string)r["highPriorityData"]);
                                            }
                                            break;
                                        }
                                        conn.Close();
                                    }

                                    lock (room.lowPriorityKey) {
                                        room.lowPriorityData[itemId] = lpData;
                                        room.lowPriorityDataUpdateTimes[itemId] = new Dictionary<string, float>();
                                    }
                                    
                                    lock (room.highPriorityKey) {
                                        room.highPriorityData[itemId] = hpData;
                                        room.highPriorityDataUpdateTimes[itemId] = new Dictionary<string, float>();
                                    }                               

                                    lock(room.itemEvents) {
                                        room.itemEvents[itemId] = new List<int>();
                                    }
                                }
                            }

                            if(added) {
                                //notify all
                                Dictionary<byte, object> p = new Dictionary<byte, object>();
                                p.Add((byte)ParameterKey.itemId, itemId);
                                p.Add((byte)ParameterKey.realTimeData, room.realTimeData[itemId]);
                                p.Add((byte)ParameterKey.lowPriorityData, room.lowPriorityData[itemId]);
                                p.Add((byte)ParameterKey.highPriorityData, room.highPriorityData[itemId]);
                                PlayerEvent ev = new PlayerEvent(
                                    new EventData() {
                                        Code = (byte)EventCode.itemAddedToRoom,
                                        Parameters = p
                                    },
                                    this.sendParameters
                                );
                                ev.handle(room.getPlayers());
                            }
                        }
                    }
                }
            }
        }

        private void removeItemFromRoom() {
            //item id required
            if(!this.parameterExists(ParameterKey.itemId)) {
                throw new Exception("Item id required");
            }
            int itemId = this.getParameter<int>(ParameterKey.itemId);

            Application app = Application.get();
            //if the player is in a room
            lock (app.peerToRoom) {
                if(app.peerToRoom.ContainsKey(this.peer.ConnectionId)) {
                    int roomId = app.peerToRoom[this.peer.ConnectionId];
                    lock (app.rooms) {
                        if(app.rooms.ContainsKey(roomId)) {
                            Room room = app.rooms[roomId];

                            bool removed = false;

                            lock(room.itemKey) {
                                if(room.items.Contains(itemId)) {
                                    removed = true;
                                    room.items.Remove(itemId);
                                    lock(room.eventKey) {
                                        List<int> ids = new List<int>(room.itemEvents[itemId]);
                                        room.itemEvents.Remove(itemId);
                                        foreach(int id in ids) {
                                            room.storedEvents.Remove(id);
                                        }
                                    }
                                    lock(room.realTimeKey) {
                                        room.realTimeData.Remove(itemId);
                                        room.realTimeDataUpdateTimes.Remove(itemId);
                                    }
                                    lock(room.highPriorityKey) {
                                        room.highPriorityData.Remove(itemId);
                                        room.highPriorityDataUpdateTimes.Remove(itemId);
                                    }
                                    lock(room.lowPriorityKey) {
                                        room.lowPriorityData.Remove(itemId);
                                        room.lowPriorityDataUpdateTimes.Remove(itemId);
                                        room.lowPriorityDataQueue.Remove(itemId);
                                    }
                                }
                            }

                            if(removed) {
                                //notify all
                                Dictionary<byte, object> p = new Dictionary<byte, object>();
                                p.Add((byte)ParameterKey.itemId, itemId);
                                PlayerEvent ev = new PlayerEvent(
                                    new EventData() {
                                        Code = (byte)EventCode.itemRemovedFromRoom,
                                        Parameters = p
                                    },
                                    this.sendParameters
                                );
                                ev.handle(room.getPlayers());
                            }

                            this.sendSimpleResponse();
                        }
                    }
                }
            }
        }

        private void createOrJoinRoom() {
            //room id required
            int roomId = 0;
            if (!this.parameterExists(ParameterKey.roomId)) {
                throw new Exception("Room id is not specified");
            } else {
                roomId = this.getParameter<int>(ParameterKey.roomId);
            }
            //player data is required
            Hashtable playerHash = new Hashtable();
            if (!this.parameterExists(ParameterKey.playerData)) {
                throw new Exception("Player data is not set");
            } else {
                playerHash = this.getParameter<Hashtable>(ParameterKey.playerData);
            }

            //regardless of what happens, set the peer's player data
            this.peer.data.Populate(playerHash);

            Application app = Application.get();
            //set the peerToRoom
            lock (app.peerToRoom) {
                app.peerToRoom[this.peer.ConnectionId] = roomId;
            }

            //if the room doesn't exist we need to create it
            Room room = null;
            lock (app.rooms) {
                if (app.rooms.ContainsKey(roomId)) {
                    room = app.rooms[roomId];
                } else {
                    room = new Room(roomId);
                    //add the room
                    app.rooms[roomId] = room;
                }
            }

            if (room != null) {
                lock (room.peerKey) {
                    //add the peer to the room
                    room.playerPeers[this.peer.ConnectionId] = this.peer;
                    //create a player events entry
                    room.peerEvents[this.peer.ConnectionId] = new List<int>();
                    //if there is no master peer, set this peer as the master
                    if(room.masterPeerId == 0) {
                        room.masterPeerId = this.peer.ConnectionId;
                    }
                }

                //handle the stored events on this peer
                room.handleStoredEvents(this.peer);

                //create the peerJoinedRoom event
                Dictionary<byte, object> p = new Dictionary<byte, object>();
                p.Add((byte)ParameterKey.peerId, this.peer.ConnectionId);
                p.Add((byte)ParameterKey.playerData, this.peer.data.Extract());
                PlayerEvent e = new PlayerEvent(new EventData() {
                    Code = (byte)EventCode.joinedRoom,
                    Parameters = p
                }, this.sendParameters);
                lock (room.eventKey) {
                    int id = room.storedEventIndex;
                    room.storedEventIndex++;

                    //store the event
                    room.storedEvents[id] = e.getCompressed();
                    //store a reference to the player
                    List<int> ids = room.peerEvents[this.peer.ConnectionId];
                    ids.Add(id);
                    room.peerEvents[this.peer.ConnectionId] = ids;
                }

                //send the event to other peers
                e.handle(room.getPlayers(PeerTarget.other, this.peer.ConnectionId));

                //send the master peer, synchronize and update cache
                Dictionary<byte, object> rp = new Dictionary<byte, object>();
                rp.Add((byte)ParameterKey.masterPeerId, room.masterPeerId);
                rp.Add((byte)ParameterKey.itemIds, room.items);
                rp.Add((byte)ParameterKey.realTimeData, room.realTimeData);
                rp.Add((byte)ParameterKey.lowPriorityData, room.lowPriorityData);
                rp.Add((byte)ParameterKey.highPriorityData, room.highPriorityData);
                this.peer.SendOperationResponse(new OperationResponse() {
                    OperationCode = this.operationRequest.OperationCode,
                    Parameters = rp,
                }, this.sendParameters);
            } else {
                this.sendSimpleError("Unable to create room");
            }
        }

        private void leaveRoom() {
            Application app = Application.get();
            //if the player is in a room
            lock(app.peerToRoom) {
                if (app.peerToRoom.ContainsKey(this.peer.ConnectionId)) {
                    //remove the above
                    int roomId = app.peerToRoom[this.peer.ConnectionId];
                    app.peerToRoom.Remove(this.peer.ConnectionId);

                    lock(app.rooms) {
                        if (app.rooms.ContainsKey(roomId)) {
                            Room room = app.rooms[roomId];
                            //remove the peer
                            List<int> eventIds = new List<int>();
                            lock (room.peerKey) {
                                room.playerPeers.Remove(this.peer.ConnectionId);
                                eventIds = room.peerEvents[this.peer.ConnectionId];
                                //remove the above
                                room.peerEvents.Remove(this.peer.ConnectionId);
                                //if the peer is the master peer
                                if(room.masterPeerId == this.peer.ConnectionId) {
                                    if(room.playerPeers.Count > 0) {
                                        foreach(KeyValuePair<int, PlayerPeer> pair in room.playerPeers) {
                                            room.masterPeerId = pair.Value.ConnectionId;
                                            break;
                                        }
                                    } else {
                                        room.masterPeerId = 0;
                                    }
                                }
                            }

                            //remove the events
                            lock (room.eventKey) {
                                eventIds.ForEach(delegate (int id) {
                                    room.storedEvents.Remove(id);
                                });
                            }

                            //create and handle the event to all peers
                            Dictionary<byte, object> p = new Dictionary<byte, object>();
                            p.Add((byte)ParameterKey.peerId, this.peer.ConnectionId);
                            p.Add((byte)ParameterKey.masterPeerId, room.masterPeerId);
                            new PlayerEvent(new EventData() {
                                Code = (byte)EventCode.leftRoom,
                                Parameters = p
                            }, this.sendParameters).handle(room.getPlayers(PeerTarget.all));

                            //send simple response
                            this.sendSimpleResponse();
                        } else {
                            this.sendSimpleError("Unable to find room");
                        }
                    }
                }
            }
        }

        private void setPlayerData() {
            Application app = Application.get();

            //player data is required
            Hashtable ht = new Hashtable();
            if (!this.parameterExists(ParameterKey.playerData)) {
                throw new Exception("Player data is not specified");
            } else {
                ht = this.getParameter<Hashtable>(ParameterKey.playerData);
            }

            //regardless of what happens, set the player data
            this.peer.data.Populate(ht);

            //if the player is in a room
            lock (app.peerToRoom) {
                if (app.peerToRoom.ContainsKey(this.peer.ConnectionId)) {
                    lock(app.rooms) {
                        if (app.rooms.ContainsKey(app.peerToRoom[this.peer.ConnectionId])) {
                            Room room = app.rooms[app.peerToRoom[this.peer.ConnectionId]];
                            lock(room.peerKey) {
                                //send an event to other peers in room
                                Dictionary<byte, object> p = new Dictionary<byte, object>();
                                p.Add((byte)ParameterKey.peerId, this.peer.ConnectionId);
                                p.Add((byte)ParameterKey.playerData, this.peer.data.Extract());
                                new PlayerEvent(new EventData() {
                                    Code = (byte)EventCode.playerDataSet,
                                    Parameters = p
                                }, this.sendParameters).handle(room.getPlayers(PeerTarget.other, this.peer.ConnectionId));
                            }
                        }
                    }
                }
            }
        }

        private void RPC() {

            //item id required
            if(!this.parameterExists(ParameterKey.itemId)) {
                throw new Exception("item id required");
            }
            int itemId = this.getParameter<int>(ParameterKey.itemId);
            //rpc method required
            if(!this.parameterExists(ParameterKey.RPCMethod)) {
                throw new Exception("rpc method required");
            }
            string rpcMethod = this.getParameter<string>(ParameterKey.RPCMethod);
            //args
            object[] rpcArgs = new object[] { };
            if(this.parameterExists(ParameterKey.RPCArgs)) {
                rpcArgs = this.getParameter<object[]>(ParameterKey.RPCArgs);
            }
            //buffered
            bool rpcBuffered = false;
            if(this.parameterExists(ParameterKey.RPCBuffered)) {
                rpcBuffered = this.getParameter<bool>(ParameterKey.RPCBuffered);
            }
            //peerTarget
            PeerTarget peerTarget = PeerTarget.other;
            if(this.parameterExists(ParameterKey.peerTarget)) {
                peerTarget = this.getParameter<PeerTarget>(ParameterKey.peerTarget);
            }
            //if peer target one, must have the peer id
            int peerId = 0;
            if(peerTarget == PeerTarget.one) {
                if(!this.parameterExists(ParameterKey.peerId)) {
                    throw new Exception("when peerTarget is one, you must supply a peer id");
                }
                peerId = this.getParameter<int>(ParameterKey.peerId);
            }

            Application app = Application.get();
            //if player in room
            lock(app.peerToRoom) {
                if(app.peerToRoom.ContainsKey(this.peer.ConnectionId)) {
                    int roomId = app.peerToRoom[this.peer.ConnectionId];
                    lock(app.rooms) {
                        if(app.rooms.ContainsKey(roomId)) {
                            Room room = app.rooms[roomId];

                            //create the event
                            Dictionary<byte, object> p = new Dictionary<byte, object>();
                            p.Add((byte)ParameterKey.peerId, this.peer.ConnectionId);
                            p.Add((byte)ParameterKey.RPCMethod, rpcMethod);
                            p.Add((byte)ParameterKey.RPCArgs, rpcArgs);
                            p.Add((byte)ParameterKey.itemId, itemId);

                            EventData ed = new EventData();
                            ed.Code = (byte)EventCode.RPC;
                            ed.Parameters = p;

                            PlayerEvent pe = new PlayerEvent(ed, new SendParameters() {
                                ChannelId = (byte)Channel.important,
                                Unreliable = false
                            });

                            //send the event to target peers
                            pe.handle(room.getPlayers(peerTarget, this.peer.ConnectionId, peerId));

                            //if buffered, add to stored events
                            if(rpcBuffered) {
                                lock(room.eventKey) {
                                    room.storedEvents[room.storedEventIndex] = pe.getCompressed();

                                    if(room.itemEvents.ContainsKey(itemId)) {
                                        List<int> eventIds = room.itemEvents[itemId];
                                        eventIds.Add(room.storedEventIndex);
                                        room.itemEvents[itemId] = eventIds;
                                    }                                    

                                    room.storedEventIndex++;
                                }
                            }
                        }
                    }
                }
            }
            
        }
    }
}
