﻿using System;
using System.Collections.Generic;
using Photon.SocketServer;
using SBTH.Common.Enums;
using SBTH.Server.Helpers;
using ExitGames.Logging;
using System.Collections;

namespace SBTH.SubServer.Helpers {
    /// <summary>
    /// The operation handler is a helper class designed to handle operations. Once creating an instance of the OperationHandler class, you are able to call any handler methods.
    /// </summary>
    public class Operation : OperationBase {

        private ILogger log = LogManager.GetCurrentClassLogger();

        public Operation(PeerBase peer, OperationRequest operationRequest, SendParameters sendParameters) : base(peer, operationRequest, sendParameters) { }

        public override void handle() {
            try {
                switch (operationRequest.OperationCode) {

                    default:
                        //unrecognised request
                        this.peer.SendOperationResponse(new OperationResponse() {
                            OperationCode = operationRequest.OperationCode,
                            ReturnCode = (short)ReturnCode.OP_NOT_RECOGNISED,
                            DebugMessage = "Operation not recognised"
                        }, sendParameters);
                        break;
                }
            } catch (Exception ex) {
                //auto send error
                this.peer.SendOperationResponse(new OperationResponse() {
                    OperationCode = operationRequest.OperationCode,
                    ReturnCode = (short)ReturnCode.GENERAL_ERROR,
                    DebugMessage = ex.ToString()
                }, sendParameters);
            }
        }

        #region handlers

        #endregion

        private void simpleResponse() {
            this.peer.SendOperationResponse(new OperationResponse() {
                OperationCode = this.operationRequest.OperationCode
            }, this.sendParameters);
        }
    }
}
