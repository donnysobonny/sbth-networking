﻿using Photon.SocketServer;
using SBTH.Server.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBTH.SubServer.Helpers {
    public class PlayerEvent : EventBase {
        public PlayerEvent(EventData eventData, SendParameters sendParameters) : base(eventData, sendParameters, Application.get()) { }

        public void handle(PlayerPeer targetPeer) {
            targetPeer.SendEvent(this.eventData, this.sendParameters);
        }

        public void handle(Dictionary<int, PlayerPeer> targetPeers) {
            foreach (KeyValuePair<int, PlayerPeer> pair in targetPeers) {
                pair.Value.SendEvent(this.eventData, this.sendParameters);
            }
        }

        public void broadcast(Dictionary<int, PlayerPeer> targetPeers) {
            List<PlayerPeer> peers = new List<PlayerPeer>();
            foreach (KeyValuePair<int, PlayerPeer> pair in targetPeers) {
                peers.Add(pair.Value);
            }
            this.application.BroadCastEvent(this.eventData, peers, this.sendParameters);
        }
    }
}
